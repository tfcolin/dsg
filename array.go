package dsg

func LocToInd (size []int, loc []int) int {
	dim := len(size)
	ind := loc[0]
	for i := 1; i < dim; i ++ {
		ind *= size[i]
		ind += loc[i]
	}
	return ind
}

func TotalSize (size []int) int {
	res := 1
	for _, s := range size {
		res *= s
	}
	return res
}

func IndexArray1D (data []float64, size []int) []float64 {
      return data[:size[0]]
}

func IndexArray2D (data []float64, size []int) [][]float64 {
      res := make ([][]float64, size[0])
      sind := 0;
      sind_up := size[1];

	for i := 0; i < size[0]; i ++ {
            res[i] = IndexArray1D (data[sind:], size[1:])
            sind += sind_up
	}
	return res
}

func IndexArray3D (data []float64, size []int) [][][]float64 {
      res := make ([][][]float64, size[0])
      sind := 0;
      sind_up := 1;
      for i := 1; i < 3; i ++ {
            sind_up *= size[i]
      }
	for i := 0; i < size[0]; i ++ {
            res[i] = IndexArray2D (data[sind:], size[1:])
            sind += sind_up
      }
      return res
}

func IndexArray4D (data []float64, size []int) [][][][]float64 {
      res := make([][][][]float64, size[0])
      sind := 0;
      sind_up := 1;
      for i := 1; i < 4; i ++ {
            sind_up *= size[i]
      }

	for i0 := 0; i0 < size[0]; i0 ++ {
            res[i0] = IndexArray3D (data[sind:], size[1:])
            sind += sind_up
      }
      return res
}

func IndexArray5D (data []float64, size []int) [][][][][]float64 {
      res := make([][][][][]float64, size[0])
      sind := 0;
      sind_up := 1;
      for i := 1; i < 5; i ++ {
            sind_up *= size[i]
      }

      for i0 := 0; i0 < size[0]; i0 ++ {
            res[i0] = IndexArray4D(data[sind:], size[1:])
            sind += sind_up
      }
      return res
}


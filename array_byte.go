package dsg

func IndexBArray1D (data []byte, size []int) []byte {
      return data[:size[0]]
}

func IndexBArray2D (data []byte, size []int) [][]byte {
      res := make ([][]byte, size[0])
      sind := 0;
      sind_up := size[1];

	for i := 0; i < size[0]; i ++ {
            res[i] = IndexBArray1D (data[sind:], size[1:])
            sind += sind_up
	}
	return res
}

func IndexBArray3D (data []byte, size []int) [][][]byte {
      res := make ([][][]byte, size[0])
      sind := 0;
      sind_up := 1;
      for i := 1; i < 3; i ++ {
            sind_up *= size[i]
      }
	for i := 0; i < size[0]; i ++ {
            res[i] = IndexBArray2D (data[sind:], size[1:])
            sind += sind_up
      }
      return res
}

func IndexBArray4D (data []byte, size []int) [][][][]byte {
      res := make([][][][]byte, size[0])
      sind := 0;
      sind_up := 1;
      for i := 1; i < 4; i ++ {
            sind_up *= size[i]
      }

	for i0 := 0; i0 < size[0]; i0 ++ {
            res[i0] = IndexBArray3D (data[sind:], size[1:])
            sind += sind_up
      }
      return res
}

func IndexBArray5D (data []byte, size []int) [][][][][]byte {
      res := make([][][][][]byte, size[0])
      sind := 0;
      sind_up := 1;
      for i := 1; i < 5; i ++ {
            sind_up *= size[i]
      }

      for i0 := 0; i0 < size[0]; i0 ++ {
            res[i0] = IndexBArray4D(data[sind:], size[1:])
            sind += sind_up
      }
      return res
}


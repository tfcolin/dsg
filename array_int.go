package dsg

func IndexIArray1D (data []int, size []int) []int {
      return data[:size[0]]
}

func IndexIArray2D (data []int, size []int) [][]int {
      res := make ([][]int, size[0])
      sind := 0;
      sind_up := size[1];

	for i := 0; i < size[0]; i ++ {
            res[i] = IndexIArray1D (data[sind:], size[1:])
            sind += sind_up
	}
	return res
}

func IndexIArray3D (data []int, size []int) [][][]int {
      res := make ([][][]int, size[0])
      sind := 0;
      sind_up := 1;
      for i := 1; i < 3; i ++ {
            sind_up *= size[i]
      }
	for i := 0; i < size[0]; i ++ {
            res[i] = IndexIArray2D (data[sind:], size[1:])
            sind += sind_up
      }
      return res
}

func IndexIArray4D (data []int, size []int) [][][][]int {
      res := make([][][][]int, size[0])
      sind := 0;
      sind_up := 1;
      for i := 1; i < 4; i ++ {
            sind_up *= size[i]
      }

	for i0 := 0; i0 < size[0]; i0 ++ {
            res[i0] = IndexIArray3D (data[sind:], size[1:])
            sind += sind_up
      }
      return res
}

func IndexIArray5D (data []int, size []int) [][][][][]int {
      res := make([][][][][]int, size[0])
      sind := 0;
      sind_up := 1;
      for i := 1; i < 5; i ++ {
            sind_up *= size[i]
      }

      for i0 := 0; i0 < size[0]; i0 ++ {
            res[i0] = IndexIArray4D(data[sind:], size[1:])
            sind += sind_up
      }
      return res
}


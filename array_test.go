package dsg

import (
      "fmt"
      "testing"
)

func TestByteArray (t * testing.T) {
      dim := []int{5, 6, 7}
      a_ := make ([]byte, 210)

      a := IndexBArray3D (a_, dim)

      for i := 0; i < 210; i ++ {
            a_[i] = byte(i)
      }

      for i := 0; i < dim[0]; i ++ {
            fmt.Printf ("i = %d:\n", i)
            for j := 0; j < dim[1]; j ++ {
                  fmt.Printf ("j = %d : ", j)
                  for k := 0; k < dim[2]; k ++ {
                        fmt.Printf ("%5d", a[i][j][k])
                  }
                  fmt.Printf ("\n")
            }
            fmt.Printf ("\n")
      }
}

func TestIntArray (t * testing.T) {
      dim := []int{3, 4, 5}
      a_ := make ([]int, 60)

      a := IndexIArray3D (a_, dim)

      for i := 0; i < 60; i ++ {
            a_[i] = i * 20
      }

      for i := 0; i < dim[0]; i ++ {
            fmt.Printf ("i = %d:\n", i)
            for j := 0; j < dim[1]; j ++ {
                  fmt.Printf ("j = %d : ", j)
                  for k := 0; k < dim[2]; k ++ {
                        fmt.Printf ("%5d", a[i][j][k])
                  }
                  fmt.Printf ("\n")
            }
            fmt.Printf ("\n")
      }
}

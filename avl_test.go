package dsg

import (
	"fmt"
	"testing"
)

/*
func TestAvlTree(t *testing.T) {

	const (
		n  int = 15
		n1 int = 5
	)

	numbers := [n]int{5, 3, -1, 2, 6, 7, 9, 0, -4, -5, 4, -6, 10, 8, 11}
	rm_num := [5]int{0, -1, -5, 9, 2}

	var at *AvlTree = InitIntAvlTree()

	for i := 0; i < n; i++ {
		at.Add(numbers[i])
		fmt.Printf("After Adding %10d: \n", numbers[i])
		at.Print()
		fmt.Print("\n")
	}

	fmt.Print("\n")

	for i := 0; i < n1; i++ {
		at.Remove(rm_num[i])
		fmt.Printf("After Remove %10d: \n", rm_num[i])
		at.Print()
		fmt.Print("\n")
	}

}
*/

func TestAvlDoubleRotate(t *testing.T) {

	const (
		n  int = 13
		n1 int = 1
	)

	numbers := [n]int{5, 2, 20, 1, 3, 10, 40, 0, 8, 15, 30, 50, 7}
	rm_num := [n1]int{0}

	var at *AvlTree = InitIntAvlTree()

	for i := 0; i < n; i++ {
		at.Add(numbers[i])
		fmt.Printf("After Adding %10d: \n", numbers[i])
		at.Print()
		fmt.Print("\n")
	}

	fmt.Print("\n")

	sr_num := []int{15, 18, 30, 28, -1}
	for _, sr := range sr_num {
		sp := at.Search (sr)
		fmt.Println ("Search = ", sr, "Path = ", sp)
	}

	srr_num := []int{18, 24, 30, -1, 100, 9, 13, 1}
	for _, sr := range srr_num {
		c, l, r := at.SearchRange (sr)
		var ci, li, ri int
		if l != nil {
			li = l.(int)
		} else {
			li = -1
		}
		if r != nil {
			ri = r.(int)
		} else {
			ri = -1
		}
		if c != nil {
			ci = c.(int)
		} else {
			ci = -1
		}
		fmt.Println ("Search = ", sr, "Range = ", []int{ci, li, ri})
	}

	for i := 0; i < n1; i++ {
		at.Remove(rm_num[i])
		fmt.Printf("After Remove %10d: \n", rm_num[i])
		at.Print()
		fmt.Print("\n")
	}

	fmt.Print("\n")

}

func TestAvlBug (t *testing.T) {

	numbers := []int{0, -100, 100, -150, -50, 50, 150, 25, 75, 125, 175}
	rm_num  := []int{-50, -150, 75}

      n := len(numbers)
      n1 := len(rm_num)

	var at *AvlTree = InitIntAvlTree()

	for i := 0; i < n; i++ {
		at.Add(numbers[i])
	}

      fmt.Println ("After Add:")
      at.Print()
      fmt.Println ("")

      for i := 0; i < n1; i ++ {
            at.Remove (rm_num[i])
            fmt.Println ("After Remove: ", rm_num[i])
            at.Print()
            fmt.Println ("")
      }
}

func TestAvlBugVerify (t *testing.T) {

	numbers := []int{0, -100, 100, -150, -50, 50, 150, 25, 75, 125, 175}
	rm_num  := []int{-50, -150}

      n := len(numbers)
      n1 := len(rm_num)

	var at *AvlTree = InitIntAvlTree()

	for i := 0; i < n; i++ {
		at.Add(numbers[i])
	}

      fmt.Println ("After Add:")
      at.Print()
      fmt.Println ("")

      for i := 0; i < n1; i ++ {
            at.Remove (rm_num[i])
            fmt.Println ("After Remove: ", rm_num[i])
            at.Print()
            fmt.Println ("")
      }

      at.Add (-200)
      at.Remove (125)
      at.Remove (175)

      fmt.Println ("Final: ")
      at.Print()
      fmt.Println ("")

}

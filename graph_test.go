package dsg

import (
	"testing"
	"fmt"
	"os"
)

func TestGraph (t * testing.T) {
	dg := InitDirGraph (15)
	for i := 0; i < 12; i ++ {
		dg.AddNode (i)
	}

	dg.AddEdge (2, 1)
	dg.AddEdge (1, 5)
	dg.AddEdge (5, 7)
	dg.AddEdge (7, 8)
	dg.AddEdge (8, 5)
	dg.AddEdge (6, 2)
	dg.AddEdge (9, 8)
	dg.AddEdge (8, 5)
	dg.AddEdge (3, 6)
	dg.AddEdge (6, 3)
	dg.AddEdge (3, 10)
	dg.AddEdge (6, 10)
	dg.AddEdge (10, 9)
	dg.AddEdge (0, 11)

	n := dg.GetSize()
	unuse := dg.GetUnuseIndex()
	fmt.Printf ("size = %d, unuse = %d\n", n, unuse)

	f1, _ := os.Create ("g1.dat")
	dg.Save (f1)

	dg.RemoveEdge (6, 2)
	dg.RemoveEdge (10, 9)
	dg.AddEdge (0, 4)
	dg.AddNode (12)
	dg.AddEdge (8, 12)

	f2, _ := os.Create ("g2.dat")
	dg.Save (f2)

	dg.RemoveNode (8)

	n = dg.GetSize()
	unuse = dg.GetUnuseIndex()
	fmt.Printf ("size = %d, unuse = %d\n", n, unuse)

	f3, _ := os.Create ("g3.dat")
	dg.Save (f3)

	dg.Expand (2)

	dg.AddNode (16)
	dg.AddEdge (8, 16)
	dg.AddEdge (9, 16)
	dg.AddEdge (10, 16)
	dg.AddEdge (16, 12)

      fmt.Println ("List Node and Edges:")
      nodes := dg.GetAllNode ()
      for _, node := range nodes {
            fmt.Printf ("Node = %d\n", node)
            fmt.Printf ("     InEdge = %v\n", dg.GetInEdge (node))
            fmt.Printf ("     OutEdge = %v\n", dg.GetOutEdge (node))
      }
      fmt.Printf ("\n")
      fmt.Printf ("All Edges:\n")
      fmt.Println (dg.GetAllEdge())
      fmt.Printf ("\n")

	f4, _ := os.Create ("g4.dat")
	dg.Save (f4)

	f1i, _ := os.Open ("g1.dat")
	dg1 := LoadDirGraph (f1i)
	f2i, _ := os.Open ("g2.dat")
	dg2 := LoadDirGraph (f2i)
	f3i, _ := os.Open ("g3.dat")
	dg3 := LoadDirGraph (f3i)
	f4i, _ := os.Open ("g4.dat")
	dg4 := LoadDirGraph (f4i)

	dg1.Save (os.Stdout)
	fmt.Printf ("\n")
	dg2.Save (os.Stdout)
	fmt.Printf ("\n")
	dg3.Save (os.Stdout)
	fmt.Printf ("\n")
	dg4.Save (os.Stdout)
	fmt.Printf ("\n")

}


package dsg

import "testing"
import "fmt"
import "os"

func PrintLM (lm * LinkMat) {

	size := lm.size
	// test get label
	fmt.Printf ("GetLabel: \n")
	for i := 0; i < size; i ++ {
		for j := 0; j < size; j ++ {
			fmt.Printf ("%t ", lm.GetLabel (i, j))
		}
		fmt.Printf ("\n")
	}
	fmt.Printf ("\n")

	// test get num
	fmt.Printf ("Num: %d\n", lm.GetNum ())
	fmt.Printf ("	Row: ")
	for i := 0; i < size; i ++ {
		fmt.Printf ("%d ", lm.GetRowNum (i))
	}
	fmt.Printf("\n")
	fmt.Printf ("	Col: ")
	for i := 0; i < size; i ++ {
		fmt.Printf ("%d ", lm.GetColNum (i))
	}
	fmt.Printf("\n\n")

	// test row traverse 
	fmt.Printf ("Row traverse.\n")
	for i := 0; i < size; i ++ {
		fmt.Printf ("%d : ", i)
		for lm.RowTraverseStart(i); ; lm.RowTraverseForward() {
			row, col := lm.GetRowTraverseLabel()
			if col == -1 {break}
			fmt.Printf ("(%d, %d) ", row, col)
		}
		fmt.Printf ("\n")
	}
	fmt.Printf("\n")

	// test col traverse 
	fmt.Printf ("Col traverse.\n")
	for i := 0; i < size; i ++ {
		fmt.Printf ("%d : ", i)
		for lm.ColTraverseStart(i); ; lm.ColTraverseForward() {
			row, col := lm.GetColTraverseLabel()
			if row == -1 {break}
			fmt.Printf ("(%d, %d) ", row, col)
		}
		fmt.Printf ("\n")
	}
	fmt.Printf("\n")

	// test all traverse 

	fmt.Printf ("All traverse.\n")
	for lm.TraverseStart (); ;lm.TraverseForward () {
		row, col := lm.GetTraverseLabel()
		if row == -1 {break}
		fmt.Printf ("(%d, %d)\n", row, col)
	}
	fmt.Printf("\n")

      fmt.Println ("GetRow:")
      for i := 0; i < size; i ++ {
            fmt.Printf ("GetRow: %d\n", i)
            fmt.Println (lm.GetRow (i))
      }
      fmt.Println ("")

      fmt.Println ("GetCol:")
      for i := 0; i < size; i ++ {
            fmt.Printf ("GetCol: %d\n", i)
            fmt.Println (lm.GetCol (i))
      }
      fmt.Println ("")

      fmt.Println ("GetAllLabel:")
      fmt.Println (lm.GetAllLabel ())
      fmt.Println ("")
}

func TestLinkMat (t * testing.T) {

	lm := InitLinkMat(5)

	lm.Set (1, 0)
	lm.Set (0, 2)
	lm.Set (1, 2)
	lm.Set (3, 0)
	lm.Set (1, 3)
	lm.Set (3, 1)
	lm.Set (2, 3)
	lm.Set (2, 4)
	lm.Set (4, 3)
	lm.Set (4, 4)

	fmt.Printf ("Finish init LinkMat.\n\n")

	PrintLM (lm)

	// expand

	lm.Expand (2)

	lm.Set (5, 6)
	lm.Set (6, 5)

	lm.Set (6, 0)
	lm.Set (5, 1)
	lm.Set (6, 1)
	lm.Set (5, 4)

	lm.Set (4, 5)
	lm.Set (3, 5)
	lm.Set (3, 6)
	lm.Set (2, 6)
	lm.Set (1, 5)

	fmt.Printf ("Finish expand LinkMat.\n\n")

	PrintLM (lm)

	// unset

	lm.UnSet (1, 0)
	lm.UnSet (2, 3)
	lm.UnSet (3, 1)
	lm.UnSet (6, 1)
	lm.UnSet (3, 5)
	lm.UnSet (5, 6)

	fmt.Printf ("Finish unset LinkMat.\n\n")

	PrintLM (lm)

	fout, _ := os.Create ("lm_save.dat")
	lm.Save (fout)

	fin, _ := os.Open ("lm_load.dat")
	lm1 := LoadLinkMat (fin)
	PrintLM (lm1)
}


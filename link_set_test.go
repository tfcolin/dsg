package dsg

import "fmt"
import "os"
import "testing"

func TestLinkSet (t * testing.T) {
      ls := InitLinkSet (30)

      for i := 0; i < 10; i ++ {
            ls.Set (i * 3)
      }

      ls.UnSet (5)
      ls.UnSet (6)
      ls.UnSet (7)
      ls.UnSet (8)
      ls.UnSet (9)

      fmt.Println ("Num = ", ls.GetNum())
      fmt.Println ("")
      fmt.Println ("Traverse Forward")
      for ls.TraverseStart(); ls.GetTraverseLabel() != -1; ls.TraverseForward() {
            fmt.Println (ls.GetTraverseLabel())
      }
      fmt.Println ("")
      fmt.Println ("Traverse Backward")
      for ls.TraverseEnd(); ls.GetTraverseLabel() != -1; ls.TraverseBackward() {
            fmt.Println (ls.GetTraverseLabel())
      }
      fmt.Println ("")

      ls.Set (10)
      ls.Set (11)
      ls.Set (20)
      ls.Set (21)
      ls.Set (22)

      ls.UnSet (11)
      ls.UnSet (18)
      ls.UnSet (21)
      ls.UnSet (0)
      ls.UnSet (3)

      fmt.Println ("Num = ", ls.GetNum())
      fmt.Println ("")
      fmt.Println ("Traverse Forward")
      for ls.TraverseStart(); ls.GetTraverseLabel() != -1; ls.TraverseForward() {
            fmt.Println (ls.GetTraverseLabel())
      }
      fmt.Println ("")
      fmt.Println ("Traverse Backward")
      for ls.TraverseEnd(); ls.GetTraverseLabel() != -1; ls.TraverseBackward() {
            fmt.Println (ls.GetTraverseLabel())
      }
      fmt.Println ("")

	fout, _ := os.Create ("ls_save.dat")
	ls.Save (fout)

	fin, _ := os.Open ("ls_load.dat")
	ls1 := LoadLinkSet (fin)
	ls1.Save (os.Stdout)

      fmt.Println ("All labels: ")
      fmt.Println (ls1.GetAllLabel ())
}

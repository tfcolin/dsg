package dsg

type LListD []Value

func InitLListD () * LListD {
      ll := make (LListD, 0)
      return &ll
}

func (ll * LListD) Push (value Value) {
      *ll = append (*ll, value)
}

func (ll * LListD) Pop () (value Value) {
      l := len(*ll)
      if (l == 0) {
            return nil
      }
      value = (*ll)[l - 1]
      *ll = (*ll)[:l - 1]
      return 
}

func (ll * LListD) PopFirst () (value Value) {
      l := len(*ll)
      if (l == 0) {
            return nil
      }
      value = (*ll)[0]

      *ll = (*ll)[1:]

      return 
}

func (ll * LListD) Get (ind int) (value * Value) {
      if ind < 0 || ind >= len(*ll) {
            return nil
      }
      return &((*ll)[ind])
}

func (ll * LListD) GetFirst () (value * Value) {
      if len(*ll) == 0 {
            return nil
      }
      return ll.Get(0)
}

func (ll * LListD) GetLast () (value * Value) {
      if len(*ll) == 0 {
            return nil
      }
      return ll.Get(len(*ll) - 1)
}

func (ll * LListD) GetN () int {
      return len (*ll)
}

func (ll * LListD) Flush () {
      *ll = make ([]Value, 0)
}

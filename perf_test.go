package dsg

import (
	"testing"
	"os"
)

func TestPerf (t * testing.T) {

      pg := InitPerfGraph (9)

      pg.AddEdge (1, 2, 4)
      pg.AddEdge (2, 3, 5)
      pg.AddEdge (2, 6, 2)
      pg.AddEdge (2, 4, 3)
      pg.AddEdge (3, 4, 2)
      pg.AddEdge (4, 6, 4)
      pg.AddEdge (3, 5, 1)
      pg.AddEdge (6, 7, 2)
      pg.AddEdge (5, 8, 3)
      pg.AddEdge (7, 8, 4)

      fbefore, _ := os.Create ("before.dat")
      fafter, _ := os.Create ("after.dat")
      f2, _ := os.Create ("load.dat")

      pg.Save (fbefore)

      pg.CalNodeTimeRange ()
      pg.Save (fafter)

      f1, _ := os.Open ("after.dat")

      pg2 := LoadPerfGraph (f1)
      pg2.Save (f2)

}

